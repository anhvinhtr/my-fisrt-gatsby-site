import * as React from 'react'
import Layout from '../components/layout'

const Gioithieu = () => {
  return (
    <Layout pageTitle="Giới thiệu">
        <p>Hi there! I'm the proud creator of this site, which I built with Gatsby.</p>
    </Layout>
  )
}

export default Gioithieu